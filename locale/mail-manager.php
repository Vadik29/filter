<?php
/**
 * Формат подставновки переменных через {0} {1} {2} ... {n}
 * Например:
 *     'MODEL_VOCAB_MSG' =>  'Поле "{0}" должны быть "{1}". Разделите по "{2}".'
 * Использование:
 *     PHP:
 *         t('key', array(mixed $val0 [, mixed $... ]))
 *     JavaScript:
 *         t('key', val0 [, val])
 */

declare(strict_types = 0);

return array(
    'MM_TITLE' => 'Менеджер почтовых рассылок',
    'MM_ACCRED_CATEGORY_EXISTS' => 'Эта сфера деятельности уже выбрана',
    'MM_TEMPLATE_SUBSCRIPTION_EXISTS' => 'Подписка на эту рассылку уже существует',
    'MM_TEMPLATE_SUBSCRIPTION' => 'Подписка на рассылки',
    'MM_MAIN_EMAIL' => 'Основной email',
    'MM_ADDITIONAL_EMAIL' => 'Дополнительный email',
    'MM_TPL_NAME' => 'Наименование',
    'MM_SELECTED_CATEGORIES' => 'Выбранные сферы деятельности',
    'MM_NO_CATEGORIES_SELECTED' => 'Сферы деятельности не выбраны',
    'MM_CATEGORY_ADDED' => 'Сфера деятельности добавленаACCRED_CATEGORY',
    'MM_CATEGORY_ALREADY_SELECTED' => 'Эта сфера деятельности уже выбрана',
    'MM_CATEGORY_LIST' => 'Перечень сфер деятельности',
    'MM_NOTIFICATIONS_CONTAINING_NSI_CATEGORIES' => 'Уведомления содержащие сферы деятельности',
    'MM_SELECT_NSI_CATEGORIES' => 'Выбрать сферы деятельности',
    'MM_FILTER_MESSAGE_ERROR' => 'Ошибка в работе брокера сообщений для фильтрации рассылки. ' .
        'E-mail сообщения не были отправлены.',
    'MM_FILTER_PUBLISHER_ERROR' => 'Ошибка в работе брокера сообщений для фильтрации рассылки. ' .
        'Не все сообщения были доставлены брокеру',
    'MM_NOT_SPECIFIED' => 'Не указан',
    'MM_CONTRAGENT_OF_SUBSCRIBER_NOT_FOUND' => 'Контрагент подписчика не найден',
);
