<?php
/**
 * Publisher сообщений RabbitMQ. Много комментариев можно найти на https://ruseller.com/lessons.php?rub=37&id=2172
 */

declare(strict_types = 1);

namespace MailManager\Broker\Rabbitmq;

use PhpAmqpLib\Message\AMQPMessage;

/**
 *  Класс Publisher.
 */
class Publisher extends Common
{
    /**
     * Инициализация обмена сообщениями.
     *
     * @return void
     */
    public function init()
    {
        parent::init();
        $this->channel->confirm_select();
        $this->channel->set_nack_handler($this->getPublisErrorHandler());
    }

    /**
     * Создаёт объект сообщения серверу брокера.
     *
     * @param array $data Данные e-mail сообщения.
     *
     * @return AMQPMessage Объект сообщения серверу брокера.
     */
    public function createMessage(array $data)
    {
        return new AMQPMessage(json_encode($data), ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]);
    }

    /**
     * Отправка сообщения в очередь.
     *
     * @param AMQPMessage $message  Сообщение.
     *
     * @return void
     */
    public function publicMessage(AMQPMessage $message)
    {
        $this->channel->basic_publish($message, self::EXCHANGE_NAME);

    }

    /**
     * Добавление сообщения в очередь.
     *
     * @param array $data Данные e-mail сообщения.
     *
     * @return void
     */
    public function createNotifyTask($data)
    {
        $this->publicMessage($this->createMessage($data));
    }

    /**
     * Деструктор.
     */
    public function __destruct()
    {
        if ($this->isFilterEnable()) {
            $this->channel->wait_for_pending_acks();
            $this->closeConnection();
        }
    }
}
