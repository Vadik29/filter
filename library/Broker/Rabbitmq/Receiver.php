<?php
/**
 * Receiver сообщений RabbitMQ. Много комментариев можно найти на https://ruseller.com/lessons.php?rub=37&id=2172
 */

declare(strict_types = 1);

namespace MailManager\Broker\Rabbitmq;

/**
 *  Класс Receiver.
 */
class Receiver extends Common
{
    /**
     * Инициализация обмена сообщениями.
     *
     * @return void
     */
    public function init()
    {
        parent::init();
        $this->channel->queue_declare(self::QUEUE_NAME, false, true, false, false);
        $this->channel->queue_bind(self::QUEUE_NAME, self::EXCHANGE_NAME);
    }

    /**
     * Установка обработчика сообщений из очереди.
     *
     * @param function $handler Обработчик сообщений.
     *
     * @return void
     */
    public function setHandler($handler)
    {
        $callback = function($msg) use ($handler) {
            call_user_func($handler, $msg);
            $msg->ack();
        };
        $this->channel->basic_qos(null, 1, null);
        $this->channel->basic_consume(self::QUEUE_NAME, '', false, false, false, false, $callback);
        while ($this->channel->is_consuming()) {
            $this->channel->wait();
        }
    }
}
