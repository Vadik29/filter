<?php
/**
 * Общие методы для Publisher и Receiver.
 */

declare(strict_types = 1);

namespace MailManager\Broker\Rabbitmq;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Exchange\AMQPExchangeType;
use MailManager\Broker;

/**
 *  Класс Common.
 */
class Common extends Broker
{
    /**
     * Подключение к серверу брокера.
     *
     * @var AMQPStreamConnection
     */
    protected $connection;

    /**
     * Канал подключения к серверу брокера.
     *
     * @var AMQPChannel
     */
    protected $channel;

    /**
     * Создаёт подключение к серверу брокера.
     *
     * @return void
     */
    public function connect()
    {
        $this->connection = new AMQPStreamConnection(
            getConfigValue('mail-manager->broker->host'),
            getConfigValue('mail-manager->broker->port'),
            getConfigValue('mail-manager->broker->user'),
            getConfigValue('mail-manager->broker->password')
        );
    }

    /**
     * Инициализация обмена сообщениями.
     *
     * @return void
     */
    public function init()
    {
        $this->channel = $this->connection->channel();
        $this->channel->exchange_declare(self::EXCHANGE_NAME, AMQPExchangeType::FANOUT, false, true, true);
    }

    /**
     * Получение данных из сообщения.
     *
     * @param AMQPMessage $message Сообщение клиента.
     *
     * @return array Данные e-mail сообщения.
     */
    public function getDataFromMessage(AMQPMessage $message)
    {
        return json_decode($message->body, true);
    }

    /**
     * Отключение от сервера брокера.
     *
     * @return void
     */
    public function closeConnection()
    {
        $this->channel->close();
        $this->connection->close();
    }
}
