<?php
/**
 * Обработчики событий по рассылке e-mail сообщений.
 */

declare(strict_types = 1);

namespace MailManager\Core\Plugins;

use MailManager\Model\SubscriberType;
use MailManager\Model\Service\MailManager;
use MailManager\Model\Service\FilterRouter;

/**
 * Обработчики событий, связанные с фильтрацией рассылаемых уведомлений.
 */
class EventListener
{
    /**
     * Сервис управления рассылкой.
     *
     * @var MailManager
     */
    protected $mailManager;

    /**
     * Сервис направления email-сообщения в фильтр.
     *
     * @var FilterRouter
     */
    public $filterRouter;

    /**
     * EventListener constructor.
     *
     * @param MailManager|null  $mailManager  Сервис управления рассылкой.
     * @param FilterRouter|null $filterRouter Фильтр сообщений.
     *
     */
    public function __construct(MailManager $mailManager = null, FilterRouter $filterRouter = null) {
        $this->mailManager = $mailManager;
        $this->filterRouter = $filterRouter;
    }

    /**
     * Обработчик события перед отправкой сообщения на основной e-mail адрес контрагента.
     *
     * @param array $params Параметры.
     *
     * @return bool Результат отправки.
     */
    public function onNotifyContragentMainAddr(array $params)
    {
        $params[SubscriberType::KEY] = SubscriberType::CONTRAGENT_MAIN_TYPE;
        return $this->filterRouter->notify($params);
    }

    /**
     * Обработчик события перед отправкой сообщения на дополнительный e-mail адрес контрагента.
     *
     * @param array $params Параметры.
     *
     * @return bool Результат отправки.
     */
    public function onNotifyContragentAddAddr(array $params)
    {
        $params[SubscriberType::KEY] = SubscriberType::CONTRAGENT_ADDITIONAL_TYPE;
        return $this->filterRouter->notify($params);
    }

    /**
     * Отправка писем пользователям контрагента.
     *
     * @param array $params Параметры.
     *
     * @return void
     */
    public function onNotifyContragentUsers(array $params)
    {
        $params[SubscriberType::KEY] = SubscriberType::USER_MAIN_TYPE;
        $this->filterRouter->notify($params);
    }

    /**
     * Создание подписчиков для новой организации и её пользователей.
     *
     * @param \Model_Contragent|null $contragent Модель контрагента.
     *
     * @return void
     */
    public function onNewContragentRegistered(\Model_Contragent $contragent = null)
    {
        $this->mailManager->createContragentSubscribers($contragent);
    }

    /**
     * Обработчик событитя регистрации пользователя.
     *
     * @param \Model_User $user Новый пользователь системы.
     *
     * @return void
     */
    public function onNewUserRegistered(\Model_User $user)
    {
        $this->mailManager->createUserSubscriber($user);
    }
}
