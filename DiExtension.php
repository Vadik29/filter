<?php
/**
 * Собиратель сервисов модуля для DI контейнера.
 */

declare(strict_types = 1);

namespace MailManager;

use Cognitive\Cometp\Di\ModuleExtension;

/**
 * Собиратель сервисов модуля для DI контейнера.
 */
class DiExtension extends ModuleExtension
{

}
