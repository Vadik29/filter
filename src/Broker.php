<?php
/**
 * Abstract Broker для наследования.
 */

declare(strict_types = 1);

namespace MailManager;

use MailManager\Model\Service\MailManager;

/**
 *  Класс Broker.
 */
abstract class Broker
{
    // Название обмена сообщениями.
    const EXCHANGE_NAME = 'MAIL_MANAGER_EXCHANGE';

    // Название очереди сообщений.
    const QUEUE_NAME = 'MAIL_MANAGER_QUEUE';

    /**
     * Фильтровать ли сообщения контрагентов-заказчиков.
     *
     * @var Boolean
     */
    protected $filterCustomer;

    /**
     * Фильтровать ли сообщения контрагентов-поставщиков.
     *
     * @var Boolean
     */
    protected $filterSupplier;

    /**
     * Сервис управления рассылкой.
     *
     * @var MailManager
     */
    protected $mailManager;

    /**
     * Сервис логирования.
     *
     * @var Object
     */
    protected $log;

    /**
     * Конструктор.
     *
     * @param MailManager|null $mailManager Сервис управления рассылкой.
     * @param Object|null      $log         Сервис логирования.
     */
    public function __construct(MailManager $mailManager = null, $log = null)
    {
        $this->filterCustomer = (bool)getConfigValue('mail-manager->filter->customer');
        $this->filterSupplier = (bool)getConfigValue('mail-manager->filter->supplier');
        $this->mailManager = $mailManager;
        $this->log = $log;
        if ($this->isFilterEnable()) {
            try {
                $this->connect();
                $this->init();
            } catch (\Exception $e) {
                logException($e);
            }
        }
    }

    /**
     * Делать ли фильтрацию email-сообщений для контрагента-заказчика.
     *
     * @return bool Булево значение.
     */
    public function isFilterCustomer()
    {
        return $this->filterCustomer;
    }

    /**
     * Делать ли фильтрацию email-сообщений для контрагента-поставщика.
     *
     * @return bool Булево значение.
     */
    public function isFilterSupplier()
    {
        return $this->filterSupplier;
    }

    /**
     * Проверяет включать ли брокера сообщений.
     *
     * @return bool Булево значение.
     */
    public function isFilterEnable()
    {
        return $this->isFilterCustomer() || $this->isFilterSupplier();
    }

    /**
     * Возвращает функцию для обработки ошибок при передаче сообщений брокеру.
     *
     * @return function Функция для обработки ошибок.
     */
    protected function getPublisErrorHandler (){
        return function ($message) {
            $data = $this->getDataFromMessage($message);
            $this->mailManager->notify($data);
            $data = [
                'procedure_id' => $data[\Model_MailLog::PARAM_PROCEDURE_ID],
                'contragent_id' => $data[\Model_MailLog::PARAM_CONTRAGENT_ID],
                'template_name' => $data[\Model_MailLog::TPL]

            ];
            $this->log->error(t('MM_FILTER_PUBLISHER_ERROR'), $data);
        };
    }

    /**
     * Создаёт подключение к серверу брокера.
     *
     * @return void
     */
    abstract public function connect();

    /**
     * Инициализация обмена сообщениями.
     *
     * @return void
     */
    abstract public function init();

    /**
     * Отключение от сервера брокера.
     *
     * @return void
     */
    abstract public function closeConnection();

    /**
     * Деструктор.
     */
    public function __destruct()
    {
        if ($this->isFilterEnable()) {
            $this->closeConnection();
        }
    }
}
