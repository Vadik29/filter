<?php
/**
 * Модификация конфигурации проиложения перед выполнением unit-тестов.
 */

namespace MailManager\Test;

use Cometp\Tests\Application\ConfigModification as BaseConfigModification;

/**
 * Class ConfigModification
 * @package MailManager\Test
 */
class ConfigModification extends BaseConfigModification
{
    /**
     * Модификация конфигурации.
     *
     * @return void
     */
    public function modify()
    {
        /**
         * Отключение брокера сообщений, т.к. он не работает в сессии теста.
         * Из-за этого тест не находит отправленные сообщения.
         */
        $this->setConfigValue('mail-manager->filter->customer', false);
        $this->setConfigValue('mail-manager->filter->supplier', false);
    }
}
