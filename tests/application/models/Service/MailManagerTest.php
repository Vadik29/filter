<?php
/**
 * Тест работы модуля mail-manager.
 */

declare(strict_types = 1);

namespace MailManager\Test\Models\Service;

use Cometp\Tests\Fixture\MockProfile;
use Nsi\Tests\Fixture\MockNSI;
use MailManager\Model\Subscriber;
use MailManager\Model\MailTemplate;
use MailManager\Model\MailTemplateGroup;
use MailManager\Model\MailTemplateSubscription;
use MailManager\Model\NsiCategorySubscription;
use MailManager\Model\Service\MailManager;
use MailManager\Model\Service\NsiCategory;
use MailManager\Core\Plugins\EventListener;

/**
 * Class MailManager
 */
class MailManagerTest extends \BaseControllerTestCase
{
    // Событие регистрации контрагента.
    const CONTRAGENT_REGISTERED_EVENT = 'MM_MAILING_TEST_CONTRAGENT_REGISTERED_EVENT';

    /**
     * Объект для работы с тестовым профилем.
     *
     * @var MockProfile
     */
    protected $mockProfile;

    /**
     * Модель контрагента.
     *
     * @var \Model_Contragent
     */
    protected $contragent;

    /**
     * Модель пользователя.
     *
     * @var \Model_User
     */
    public $user;

    /**
     * Модель email сообщения.
     *
     * @var \Model_MailLog
     */
    protected $mailLog;

    /**
     * Хелпер для работы с классификатором.
     *
     * @var MockNSI
     */
    protected $mockNSI;

    /**
     * Модель подписчика.
     *
     * @var Subscriber
     */
    public $subscriber;

    /**
     * Категории шаблонов сообщений.
     *
     * @var MailTemplateGroup
     */
    public $mailTemplateGroup;

    /**
     * Модель шаблона сообщения.
     *
     * @var MailTemplate
     */
    public $mailTemplate;

    /**
     * Модель подписки на шаблон email-сообщения.
     *
     * @var MailTemplateSubscription
     */
    protected $mailTplSub;

    /**
     * Модель подписки на категориию сферы деятельности.
     *
     * @var NsiCategorySubscription
     */
    public $nsiCatSub;

    /**
     * Сервис управления рассылкой.
     *
     * @var MailManager
     */
    protected $mailManager;

    /**
     * Сервис категорий сфер деятельности.
     *
     * @var NsiCategory
     */
    public $nsiCategoryService;

    /**
     * Инициализация теста.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->mockProfile = new MockProfile();
        $container = $this->getContainer();
        $this->contragent = $container->get('Cometp_ModelContragent');
        $this->user = $container->get('Cometp_ModelUser');
        $this->mailLog = $container->get('Cometp_Model_MailLog');
        $this->mockNSI = new MockNSI();
        $this->subscriber = $container->get('MailManager_Model_Subscriber');
        $this->mailTemplateGroup = $container->get('MailManager_Model_MailTemplateGroup');
        $this->mailTemplate = $container->get('MailManager_Model_MailTemplate');
        $this->mailTplSub = $container->get('MailManager_Model_MailTemplateSubscription');
        $this->nsiCatSub = $container->get('MailManager_Model_NsiCategorySubscription');
        $this->mailManager = $container->get('MailManager_Model_Service_MailManager');
        $this->nsiCategoryService = $container->get('MailManager_Model_Service_NsiCategory');
        $eventListener = $container->get('MailManager_Core_Plugins_EventListener');
        // Чтобы отправить сообщение прямо в фильтр, минуя брокера сообщений.
        $eventListener->filterRouter = $this->mailManager;

    }

    /**
     * Тест отправки и получения сообщений с применением фильтра модуля.
     *
     * @return void
     */
    public function testMailing()
    {
        // Сообщение, на которое не устанавливается состояние подписки.
        $messageSimple = \Core_Template::REGISTER_SUPPLIER;
        // Сообщение, на которое устанавливается состояние подписки.
        $messageWithSubscription = \Core_Template::ACCREDITATION_SUPPLIER_ADDED;
        // Сообщение, на которое устанавливается состояние подписки и выполняется дополнительная проверка для отправки.
        $messageWithSubscriptionAndCheck = \Core_Template::ACCREDITATION_SUPPLIER_ACCEPTED;

        /** Создание тестовых данных.
         * Всего 3 подписчика:
         * основной email контрагента, дополнительный email контрагента и основной email пользователя.
        */
        $testData = $this->getTestData();
        $templateGroup = $this->mailTemplateGroup->create([
            MailTemplateGroup::PARAM_NAME => 'Сообщения поставщика',
            MailTemplateGroup::PARAM_CODE => 'REGISTER_MESSAGE'
        ]);
        $templateGroup->save();
        $mailTplWithSub = $this->mailTemplate->create([
            MailTemplate::PARAM_CODE => $messageWithSubscription,
            MailTemplate::PARAM_NAME => 'Сообщение, на которое есть возможность подписки',
            MailTemplate::PARAM_GROUP_ID => $templateGroup->getId()
        ]);
        $mailTplWithSub->save();
        $this->mailTemplate->create([
            MailTemplate::PARAM_CODE => $messageWithSubscriptionAndCheck,
            MailTemplate::PARAM_NAME => 'Сообщение, на которое есть возможность подписки и у которого определено ' .
                'свойство для выполнения проверки перед отправкой',
            MailTemplate::PARAM_GROUP_ID => $templateGroup->getId(),
            MailTemplate::PARAM_WITH_CHECK => true,
        ])->save();
        $contragent = $this->contragent->create([
            \Model_Contragent::PARAM_INN => $testData->randomInn(),
            \Model_Contragent::PARAM_KPP => $testData->randomKpp(),
            \Model_Contragent::PARAM_EMAIL => $testData->randomEmail(),
            \Model_Contragent::PARAM_EMAIL_ADD => $testData->randomEmail(),
        ], false);
        $this->user->create($testData->getUserData($testData->randomString()), $contragent->getId(), true, [], false)
        // Пользователи создаются только со статусами \Model_User::STATUS_NOT_CONFIRMED.
        ->setStatus(\Model_User::STATUS_AUTHORIZED)->save();

        /** @var array $contragentSubscribers Все подписчики организации включая пользователей.*/
        $contragentSubscribers = $this->subscriber->getSubscribersByContragent($contragent->getId());
        $mailLogDbTable = $this->mailLog->getDbTable();
        $mailLogDbTable->delete(sprintf('%s=%s', \Model_MailLog::PARAM_CONTRAGENT_ID, $contragent->getId()));

        // Тест отправки сообщения, на которое не устанавливается состояние подписки.
        $this->mailLog->notifyContragentUsingTemplate($contragent, $messageSimple, []);
        $this->assertEquals(
            3,
            $this->mailLog->findBy([\Model_MailLog::PARAM_CONTRAGENT_ID => $contragent->getId()])->count(),
            'Simple letters does not match'
        );
        $mailLogDbTable->delete(sprintf('%s=%s', \Model_MailLog::PARAM_CONTRAGENT_ID, $contragent->getId()));

        // Тест отправки сообщения, на которое устанавливается состояние подписки.
        $this->mailLog->notifyContragentUsingTemplate($contragent, $messageWithSubscription, []);
        $this->assertEquals(
            3,
            $this->mailLog->findBy([\Model_MailLog::PARAM_CONTRAGENT_ID => $contragent->getId()])->count(),
            'With subscription letters does not match'
        );
        $mailLogDbTable->delete(sprintf('%s=%s', \Model_MailLog::PARAM_CONTRAGENT_ID, $contragent->getId()));

        // Отписка от сообщения и повторная отправка.
        $params = ['tpl_id' => $mailTplWithSub->getId()];
        foreach ($contragentSubscribers as $subscriber) {
            $params[$subscriber->getId()] = false;
        }
        $this->mailTplSub->updateSubscriptions([$params]);
        $this->mailLog->notifyContragentUsingTemplate($contragent, $messageWithSubscription, []);
        $this->assertEquals(
            0,
            $this->mailLog->findBy([\Model_MailLog::PARAM_CONTRAGENT_ID => $contragent->getId()])->count(),
            'Unsubscribed messages were sended'
        );

        /** Тест отправки сообщения, на которое устанавливается состояние подписки и
         * выполняется дополнительная проверка для отправки.
         */
        $testCategory = $this->mockNSI->createTestCategory()->getCode();
        foreach ($contragentSubscribers as $subscriber) {
            $this->nsiCatSub->subscribe($subscriber, [$testCategory]);
        }
        $this->mailLog->notifyContragentUsingTemplate(
            $contragent,
            $messageWithSubscriptionAndCheck,
            [],
            [NsiCategory::CATEGORY_KEY => [$testCategory]]
        );
        $this->assertEquals(
            3,
            $this->mailLog->findBy([\Model_MailLog::PARAM_CONTRAGENT_ID => $contragent->getId()])->count(),
            'With subscription and check letters does not match'
        );
        $mailLogDbTable->delete(sprintf('%s=%s', \Model_MailLog::PARAM_CONTRAGENT_ID, $contragent->getId()));

        // Отписка от категорий сфер деятельности.
        foreach ($contragentSubscribers as $subscriber) {
            $this->nsiCatSub->unsubscribe($subscriber, [$testCategory]);
        }
        $this->mailLog->notifyContragentUsingTemplate(
            $contragent,
            $messageWithSubscriptionAndCheck,
            [],
            [NsiCategory::CATEGORY_KEY => [$testCategory]]
        );
        $this->assertEquals(
            0,
            $this->mailLog->findBy([\Model_MailLog::PARAM_CONTRAGENT_ID => $contragent->getId()])->count(),
            'Messages with unsubscribed category check were sended'
        );
    }
}
