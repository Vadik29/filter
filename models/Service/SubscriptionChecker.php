<?php
/**
 * Сервис проверки подписки на email-сообщение.
 */

declare(strict_types = 1);

namespace MailManager\Model\Service;

use MailManager\Model\Subscriber;
use MailManager\Model\MailTemplate;
use MailManager\Model\MailTemplateSubscription as MailTplSub;

/**
 * Класс сервиса
 */
class SubscriptionChecker
{
    /**
     * Модель подписчика.
     *
     * @var Subscriber
     */
    public $subscriber;

    /**
     * Модель шаблона сообщения.
     *
     * @var MailTemplate
     */
    public $mailTemplate;

    /**
     * Модель подписки на шаблон email-сообщения.
     *
     * @var MailTplSub
     */
    public $mailTplSub;

    /**
     * Сервис проверки подписки на email-сообщение по дополнительным параметрам сообщения.
     *
     * @var MailChecker
     */
    public $mailChecker;

    /**
     * SubscriptionChecker constructor.
     *
     * @param Subscriber   $subscriber   Модель подписчика.
     * @param MailTemplate $mailTemplate Модель шаблона сообщения.
     * @param MailTplSub   $mailTplSub   Модель подписки на шаблон email-сообщения.
     * @param MailChecker  $mailChecker  Сервис проверки подписки на email-сообщение по
     *                                   дополнительным параметрам сообщения.
     */
    public function __construct(
        Subscriber $subscriber,
        MailTemplate $mailTemplate,
        MailTplSub $mailTplSub,
        MailChecker $mailChecker
    ) {
        $this->subscriber = $subscriber;
        $this->mailTemplate = $mailTemplate;
        $this->mailTplSub = $mailTplSub;
        $this->mailChecker = $mailChecker;
    }

    /**
     * Проверка подписки на шаблон email-сообщения.
     *
     * @param Subscriber|null $subscriber Подписчик.
     * @param array           $data       Данные e-mail сообщения.
     *
     * @return bool Результат проверки.
     */
    public function checkSubscription(Subscriber $subscriber = null, array $data)
    {
        $templateName = getParamAsString($data, \Model_MailLog::TPL);
        if (!$subscriber || !$templateName) {
            return true;
        }
        $mailTemplate = $this->mailTemplate->findOneBy([MailTemplate::PARAM_CODE => $templateName]);
        if (!$mailTemplate) {
            return true;
        }

        // Проверяем, подписан ли подписчик на шаблон отправляемого email-сообщения.
        $tplSubscription = $this->mailTplSub->findBy([
            MailTplSub::PARAM_SUBSCRIBER_ID => $subscriber->getId(),
            MailTplSub::PARAM_TEMPLATE_ID => $mailTemplate->getId(),
            MailTplSub::PARAM_ACTIVE => true
        ]);
        if (!count($tplSubscription)) {
            return false;
        }
        if ($mailTemplate->getWithCheck()) {
            return $this->mailChecker->check($subscriber, $data);
        }
        return true;
    }
}
