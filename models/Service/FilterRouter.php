<?php
/**
 * Сервис направления email-сообщения в фильтр.
 */

declare(strict_types = 1);

namespace MailManager\Model\Service;

/**
 *  Класс FilterRouter.
 */
class FilterRouter
{
    /**
     * Модель контрагента.
     *
     * @var \Model_Contragent
     */
    public $contragent;

    /**
     * Логер.
     *
     * @var \Model_Syslog
     */
    protected $sysLog;

    /**
     * Publisher сообщений.
     *
     * @var Object
     */
    protected $publisher;

    /**
     * Конструктор.
     * @param \Model_Contragent $contragent Модель контрагента.
     * @param Object            $publisher  Publisher сообщений.
     */

    public function __construct(\Model_Contragent $contragent, \Model_Syslog $sysLog, $publisher)
    {
        $this->contragent = $contragent;
        $this->sysLog = $sysLog;
        $this->publisher = $publisher;
    }

    /**
     * Проверяет нужно ли делать фильтрацию email-сообщения.
     *
     * @param array $data Данные e-mail сообщения.
     *
     * @return bool Булево значение.
     */
    public function isNeedToFilter($data)
    {
        $contragent = $this->contragent->load(getParamAsInt($data, 'contragent_id'));
        return $contragent && ($this->publisher->isFilterCustomer() && $contragent->getCustomerProfileId() ||
        $this->publisher->isFilterSupplier() && $contragent->getSupplierProfileId());
    }

    /**
     * Принимает e-mail сообщение для направления в фильтр.
     *
     * @param array $data Данные e-mail сообщения.
     *
     * @throws \Exception Исключение.
     *
     * @return bool Булево значение.
     */
    public function notify($data)
    {
        if ($this->isNeedToFilter($data)) {
            try {
                $this->publisher->createNotifyTask($data);
                return false;
            } catch (\Exception $e) {
                $this->sysLog->log([
                    'module' => 'mail-manager',
                    'message' => t('MM_FILTER_MESSAGE_ERROR')
                ]);
                throw new \Exception(t('MM_FILTER_MESSAGE_ERROR'));
            }
        }
        return true;
    }
}
