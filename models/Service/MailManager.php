<?php
/**
 * Сервис MailManager.
 */

declare(strict_types = 1);

namespace MailManager\Model\Service;

use MailManager\Model\Subscriber;
use MailManager\Model\SubscriberType;
use MailManager\Model\MailTemplate;
use MailManager\Model\NsiCategorySubscription as NsiCatSub;
use MailManager\Model\MailTemplateSubscription as MailTplSub;

/**
 * Сервис MailManager
 */
class MailManager
{
    /**
     * Модель контрагента.
     *
     * @var \Model_Contragent
     */
    public $contragent;

    /**
     * Модель пользователя.
     *
     * @var \Model_User
     */
    public $user;

    /**
     * Модель рассылки email сообщений.
     *
     * @var \Model_MailLog
     */
    protected $mailLog;

    /**
     * Модель подписчика.
     *
     * @var Subscriber
     */
    public $subscriber;

    /**
     * Модель типа подписчиков.
     *
     * @var SubscriberType
     */
    public $subscriberType;

    /**
     * Модель шаблона сообщения.
     *
     * @var MailTemplate
     */
    public $mailTemplate;

    /**
     * Модель подписки на шаблон email-сообщения.
     *
     * @var MailTplSub
     */
    public $mailTplSub;

    /**
     * Модель подписки на категориию сферы деятельности.
     *
     * @var NsiCatSub
     */
    public $nsiCatSub;

    /**
     * Сервис категорий сфер деятельности.
     *
     * @var SubscriptionChecker
     */
    public $subCheckerService;

    /**
     * Сервис категорий сфер деятельности.
     *
     * @var object
     */
    public $nsiCategoryService;

    /**
     * MailManager constructor.
     *
     * @param \Model_Contragent   $contragent         Модель контрагента.
     * @param \Model_User         $user               Модель пользователя.
     * @param \Model_MailLog      $mailLog            Модель рассылки email сообщений.
     * @param Subscriber          $subscriber         Модель подписчика.
     * @param SubscriberType      $subscriberType     Модель типа подписчика.
     * @param MailTemplate        $mailTemplate       Модель шаблона сообщения.
     * @param MailTplSub          $mailTplSub         Модель подписки на шаблон email-сообщения.
     * @param NsiCatSub           $nsiCatSub          Модель подписки на категориию сферы деятельности.
     * @param SubscriptionChecker $subCheckerService  Сервис проверки подписки на email-сообщение.
     * @param object              $nsiCategoryService Сервис категории сфер деятельности.
     */
    public function __construct(
        \Model_Contragent $contragent,
        \Model_User $user,
        \Model_MailLog $mailLog,
        Subscriber $subscriber,
        SubscriberType $subscriberType,
        MailTemplate $mailTemplate,
        MailTplSub $mailTplSub,
        NsiCatSub $nsiCatSub,
        SubscriptionChecker $subCheckerService,
        $nsiCategoryService
    ) {
        $this->contragent = $contragent;
        $this->user = $user;
        $this->mailLog = $mailLog;
        $this->subscriber = $subscriber;
        $this->subscriberType = $subscriberType;
        $this->mailTemplate = $mailTemplate;
        $this->mailTplSub = $mailTplSub;
        $this->nsiCatSub = $nsiCatSub;
        $this->subCheckerService = $subCheckerService;
        $this->nsiCategoryService = $nsiCategoryService;
    }

    /**
     * Отправка e-mail сообщения.
     *
     * @param array $data Данные e-mail сообщения.
     *
     * @return bool Булево значение.
     */
    public function notify(array $data)
    {
        $contragent = $this->contragent->load(getParamAsInt($data, 'contragent_id'));
        $subscribers = [];
        switch ($data[SubscriberType::KEY]){
            case SubscriberType::CONTRAGENT_MAIN_TYPE:
                $subscribers[$data['email']] = $this->subscriber->getMainContragentSubscriber($contragent);
                break;
            case SubscriberType::CONTRAGENT_ADDITIONAL_TYPE:
                $subscriber = $this->subscriber->getAdditionalContragentSubscriber($contragent);
                foreach ($contragent->getEmailAddArray() as $addEmail) {
                    $subscribers[$addEmail] = $subscriber;
                }
                break;
            case SubscriberType::USER_MAIN_TYPE:
                foreach ($contragent->getUsers() as $user) {
                    $subscribers[$user->getUserEmail()] = $this->subscriber->getMainUserSubscriber($user);
                }
        }
        foreach ($subscribers as $email => $subscriber) {
            if ($this->subCheckerService->checkSubscription($subscriber, $data)) {
                $data['email'] = $email;
                $this->mailLog->log($data);
            }
        }
        return false;
    }

    /**
     * Подписать подписчика на все сферы деятельности контрагента.
     *
     * @param Subscriber        $subscriber Подписчик.
     * @param \Model_Contragent $contragent Контрагент.
     *
     * @return void
     * @throws \Exception Исключение.
     */
    public function subscribeAllNsiCategories(Subscriber $subscriber, \Model_Contragent $contragent)
    {
        // Добавляем ему все сферы деятельности организации.
        $categories = $this->nsiCategoryService->getContragentCategoryCodes($contragent);
        $this->nsiCatSub->subscribe($subscriber, $categories);
    }

    /**
     * Подписать подписчика на все шаблоны рассылки.
     *
     * @param Subscriber $subscriber Подписчик.
     *
     * @return void
     * @throws \Exception Исключение.
     */
    public function subscribeAllTemplates(Subscriber $subscriber)
    {
        // Подписываем на все уведомления.
        $templates = $this->mailTemplate->findObjectsBy([]);
        foreach ($templates as $template) {
            $this->mailTplSub->create([
                MailTplSub::PARAM_TEMPLATE_ID => $template->getId(),
                MailTplSub::PARAM_SUBSCRIBER_ID => $subscriber->getId(),
                MailTplSub::PARAM_ACTIVE => true
            ])->save();
        }
    }

    /**
     * Создание модели подписчика для нового пользователя.
     *
     * @param \Model_User $user Пользователь.
     *
     * @return Subscriber Модель подписчика.
     * @throws \Exception Исключение.
     */
    public function createUserSubscriber(\Model_User $user)
    {
        $type = $this->subscriberType->findOneBy([SubscriberType::PARAM_CODE => SubscriberType::USER_MAIN_TYPE]);
        $subscriber = $this->subscriber->createSubscriber($user, $type);
        $contragent = $user->getCompany();
        if ($contragent) {
            $this->subscribeAllNsiCategories($subscriber, $contragent);
        }
        $this->subscribeAllTemplates($subscriber);
        return $subscriber;
    }

    /**
     * Создание моделей подписчиков для новой организации и её пользователей.
     *
     * @param \Model_Contragent $contragent Модель контрагента.
     *
     * @return void
     * @throws \Exception Исключение.
     */
    public function createContragentSubscribers(\Model_Contragent $contragent)
    {
        $subscriberTypes = [
            $this->subscriberType->findOneBy([SubscriberType::PARAM_CODE => SubscriberType::CONTRAGENT_MAIN_TYPE]),
            $this->subscriberType->findOneBy([SubscriberType::PARAM_CODE => SubscriberType::CONTRAGENT_ADDITIONAL_TYPE])
        ];
        foreach ($subscriberTypes as $type) {
            $subscriber = $this->subscriber->createSubscriber($contragent, $type);
            $this->subscribeAllNsiCategories($subscriber, $contragent);
            $this->subscribeAllTemplates($subscriber);
        }
    }

    /**
     * Актуализация подписок на категории сфер деятельности.
     *
     * @param \Model_Contragent $contragent Модель контрагента.
     *
     * @return void
     */
    public function actualizeNsiCategorySubscription(\Model_Contragent $contragent)
    {
        $contragentCategories = $this->nsiCategoryService->getContragentCategoryCodes($contragent);
        $subscribers = $this->subscriber->getSubscribersByContragent($contragent->getId());
        foreach ($subscribers as $subscriber) {
            // Удаление категорий, от которых подписчик отписался ранее.
            $unsubscribedCategories = $this->nsiCatSub
                ->findBy([NsiCatSub::PARAM_SUBSCRIBER_ID => $subscriber->getId(), NsiCatSub::PARAM_ACTIVE => false]);
            foreach ($unsubscribedCategories as $category) {
                $key = array_search($category->category_code, $contragentCategories);
                if ($key !== false) {
                    unset($contragentCategories[$key]);
                }
            }
            // Обновление подписки на категории оставшимися категориями.
            $this->nsiCatSub->setSubscriptions($subscriber, $contragentCategories);
        }
    }

    /**
     * Удаляет подписки на категории сфер деятельности у контрагента и его пользователей.
     * Применяетя, когда происхдит вынужденная отписка, чтобы потом
     * при восстановлении категории произошла автоматическая подписка.
     *
     * @param array             $categoryCodes Код категории.
     * @param \Model_Contragent $contragent    Модель контрагента.
     *
     * @return void
     */
    public function deleteNsiCategorySubscruptionsByContragent(array $categoryCodes, \Model_Contragent $contragent)
    {
        $this->nsiCatSub->deleteSubscruptionsByContragent($categoryCodes, $contragent);
    }
}
