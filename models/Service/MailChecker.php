<?php
/**
 * Сервис проверки подписки на email-сообщение по дополнительным параметрам сообщения.
 */

declare(strict_types = 1);

namespace MailManager\Model\Service;

use MailManager\Model\Subscriber;
use MailManager\Model\NsiCategorySubscription as NsiCatSub;

/**
 * Класс сервиса
 */
class MailChecker
{
    /**
     * Модель подписки на категориию сферы деятельности.
     *
     * @var NsiCatSub
     */
    public $nsiCatSub;

    /**
     * MailChecker constructor.
     *
     * @param NsiCatSub $nsiCatSub Модель подписки на категориию сферы деятельности.
     */
    public function __construct(NsiCatSub $nsiCatSub) {
        $this->nsiCatSub = $nsiCatSub;
    }

    /**
     * Проверка подписки на категории сфер деятельности, указанные в сообщении.
     *
     * @param Subscriber $subscriber Подписчик.
     * @param array      $data       Данные e-mail сообщения.
     *
     * @return bool Результат проверки.
     */
    public function check(Subscriber $subscriber, array $data)
    {
        $categories = getParamAsArray($data, NsiCategory::CATEGORY_KEY);
        return $categories && $this->nsiCatSub->findObjectsBy([
            NsiCatSub::PARAM_SUBSCRIBER_ID => $subscriber->getId(),
            NsiCatSub::PARAM_CATEGORY_CODE => $data[NsiCategory::CATEGORY_KEY],
            NsiCatSub::PARAM_ACTIVE => true
        ]);
    }
}
