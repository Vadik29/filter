<?php
/**
 * Сервис категории сфер деятельности.
 */

declare(strict_types = 1);

namespace MailManager\Model\Service;

/**
 * Класс сервиса
 */
class NsiCategory
{

    const CATEGORY_KEY = 'category';

    /**
     * Возвращает массив категорий сфер деятельности контрагента. При необходимости переопределить в services.xml
     * главного модуля проекта.
     *
     * @param \Model_Contragent $contragent Модель контрагента.
     *
     * @return array Массив категорий сфер деятельности.
     */
    public function getContragentCategoryCodes(\Model_Contragent $contragent)
    {
        return [];
    }
}
