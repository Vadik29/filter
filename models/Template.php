<?php
/**
 * Модель расширения шаблонов для модуля MailManager.
 */

declare(strict_types = 1);

namespace MailManager\Model;

use Core_Template;

/**
 *  Класс Template.
 */
class Template extends Core_Template
{
    // Шаблон сообщения об ошибке при попытке подписаться на недопустимые категории сфер деятельности.
    const MM_NO_ABILITY_NSI_CATEGORY_SUBSCRIPTION = 'MM_NO_ABILITY_NSI_CATEGORY_SUBSCRIPTION';
}
