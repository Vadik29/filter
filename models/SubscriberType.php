<?php
/**
 * Модель типа подписчика.
 */

declare(strict_types = 1);

namespace MailManager\Model;

/**
 *  Класс модели.
 */
class SubscriberType extends \Core_Mapper
{

    const TABLE_NAME = DbTable\SubscriberTypes::TABLE_NAME;

    const PARAM_ID = 'id';
    const PARAM_NAME = 'name';
    const PARAM_CODE = 'code';

    const CONTRAGENT_MAIN_TYPE = 'contragent_main';
    const CONTRAGENT_ADDITIONAL_TYPE = 'contragent_additional';
    const USER_MAIN_TYPE = 'user_main';

    // Ключ для значения типа получателя.
    const KEY = 'subscriber_type';

    // @codingStandardsIgnoreStart
    protected $_dbClass = 'MailManager\Model\DbTable\SubscriberTypes';

    public $_parameters = array(
        self::PARAM_ID => array(
            'pseudo' => 'Идентификатор типа',
            'type' => self::TYPE_INT
        ),
        self::PARAM_NAME => array(
            'pseudo' => 'Название',
            'type' => self::TYPE_STRING
        ),
        self::PARAM_CODE => array(
            'pseudo' => 'Код',
            'type' => self::TYPE_STRING
        )
    );
    // @codingStandardsIgnoreEnd
}
