<?php
/**
 * Класс подписчиков.
 */

declare(strict_types = 1);

namespace MailManager\Model;

/**
 *  Класс модели.
 */
class Subscriber extends \Core_Mapper
{

    const TABLE_NAME = DbTable\Subscribers::TABLE_NAME;
    const TABLE_CLASS = 'MailManager\Model\DbTable\Subscribers';

    const PARAM_ID = 'id';
    const PARAM_OBJECT_ID = 'object_id';
    const PARAM_TYPE_ID = 'type_id';

    // @codingStandardsIgnoreStart
    protected $_dbClass = self::TABLE_CLASS;

    public $_parameters = array(
        self::PARAM_ID => array(
            'pseudo' => 'Идентификатор записи',
            'type' => self::TYPE_INT
        ),
        self::PARAM_OBJECT_ID => array(
            'pseudo' => 'Идентификатор объекта подписчика',
            'type' => self::TYPE_INT
        ),
        self::PARAM_TYPE_ID => array(
            'pseudo' => 'Тип подписчика',
            'type' => self::TYPE_INT
        )
    );

    /**
     * Модель типа подписчика.
     *
     * @var SubscriberType
     */
    public $subscriberType;

    /**
     * Создаёт экземпляр класса.
     *
     * @param SubscriberType   $subscriberType Модель типа подписчика.
     *
     * @return self $model Экземпляр класса.
     */
    public static function selfCreateObject(SubscriberType $subscriberType)
    {
        $model = new self();
        $model->subscriberType = $subscriberType;
        return $model;
    }

    /**
     * Создание подписчика.
     *
     * @param Object         $object Модель объекта подписчика.
     * @param SubscriberType $type   Модель типа подписчика.
     *
     * @return self Модель подписчика.
     */
    public function createSubscriber($object, SubscriberType $type)
    {
        $data = [
            self::PARAM_OBJECT_ID => $object->getId(),
            self::PARAM_TYPE_ID => $type->getId()
        ];
        $subscriber = $this->createObject($data);
        $subscriber->save();
        return $subscriber;
    }

    /**
     * Возвращает список подписчиков контрагента и его пользователей.
     *
     * @param int $contragentId Идентификатор контрагента.
     *
     * @return array $subscribers Список подписчиков.
     */
    public function getSubscribersByContragent(int $contragentId)
    {
        $subscribers = [];
        $db = $this->getDbTable()->getAdapter();
        $select = $db->select()->from(['s' => DbTable\Subscribers::TABLE_NAME])
            ->joinLeft(
                ['st' => DbTable\SubscriberTypes::TABLE_NAME],
                sprintf('st.%s = s.%s', SubscriberType::PARAM_ID, Subscriber::PARAM_TYPE_ID),
                []
            )
            ->joinLeft(
                ['c' => \DbTable_Contragents::NAME],
                sprintf(
                    's.%s = c.%s AND st.%s IN (\'%s\', \'%s\')',
                    Subscriber::PARAM_OBJECT_ID,
                    \Model_Contragent::PARAM_ID,
                    SubscriberType::PARAM_CODE,
                    SubscriberType::CONTRAGENT_MAIN_TYPE,
                    SubscriberType::CONTRAGENT_ADDITIONAL_TYPE
                ),
                []
            )->joinLeft(
                ['u' => \DbTable_Users::NAME],
                sprintf(
                    "s.%s = u.%s AND st.%s = '%s'",
                    Subscriber::PARAM_OBJECT_ID,
                    \Model_User::PARAM_ID,
                    SubscriberType::PARAM_CODE,
                    SubscriberType::USER_MAIN_TYPE
                ),
                []
            )->where(sprintf('c.%s = ?', \Model_Contragent::PARAM_ID), $contragentId)
            ->orWhere(sprintf('u.%s = ?', \Model_User::PARAM_CONTRAGENT_ID), $contragentId)
            ->order(sprintf('%s %s', Subscriber::PARAM_TYPE_ID, \Zend_Db_Select::SQL_ASC));
        foreach ($db->fetchAll($select) as $subscriberData) {
            $subscribers[] = $this->createObject($subscriberData);
        }
        return $subscribers;
    }

    /**
     * Возвращает модель подписчика с основным e-mail адресом контрагента.
     *
     * @param \Model_Contragent $contragent Модель контрагента.
     *
     * @return self Модель подписчика.
     */
    public function getMainContragentSubscriber(\Model_Contragent $contragent)
    {
        return $this->findOneBy(
            [
                self::PARAM_OBJECT_ID => $contragent->getId(),
                self::PARAM_TYPE_ID => $this->subscriberType
                    ->findOneBy([SubscriberType::PARAM_CODE => SubscriberType::CONTRAGENT_MAIN_TYPE])->getId()
            ]
        );
    }

    /**
     * Возвращает модель подписчика с дополнительным e-mail адресом контрагента.
     *
     * @param \Model_Contragent $contragent Модель контрагента.
     *
     * @return self Модель подписчика.
     */
    public function getAdditionalContragentSubscriber(\Model_Contragent $contragent)
    {
        return $this->findOneBy(
            [
                self::PARAM_OBJECT_ID => $contragent->getId(),
                self::PARAM_TYPE_ID => $this->subscriberType
                    ->findOneBy([SubscriberType::PARAM_CODE => SubscriberType::CONTRAGENT_ADDITIONAL_TYPE])->getId()
            ]
        );
    }

    /**
     * Возвращает модель подписчика с основным e-mail адресом пользователя.
     *
     * @param \Model_User $user Модель пользователя.
     *
     * @return self Модель подписчика.
     */
    public function getMainUserSubscriber(\Model_User $user)
    {
        return $this->findOneBy(
            [
                self::PARAM_OBJECT_ID => $user->getId(),
                self::PARAM_TYPE_ID => $this->subscriberType
                    ->findOneBy([SubscriberType::PARAM_CODE => SubscriberType::USER_MAIN_TYPE])->getId()
            ]
        );
    }

    /**
     * Возвращает объект подписчика.
     *
     * @param Subscriber $subscriber Модель подписчика.
     *
     * @return Object Объект подписчика.
     */
    public function getObjectModel(self $subscriber)
    {
        $subscriberType = $this->subscriberType->load($subscriber->getTypeId());
        if (in_array(
            $subscriberType->getCode(),
            [SubscriberType::CONTRAGENT_MAIN_TYPE, SubscriberType::CONTRAGENT_ADDITIONAL_TYPE]
        )) {
            $this->contragent->load($subscriber->getObjectId());
        } elseif ($subscriberType->getCode() == SubscriberType::USER_MAIN_TYPE) {
            $this->contragent->load($subscriber->getObjectId());
        }
    }
}
