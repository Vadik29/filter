<?php
/**
 * Класс модели шаблона email сообщений из vocab_doc_templates.
 */

declare(strict_types = 1);

namespace MailManager\Model;

/**
 *  Класс модели.
 */
class MailTemplate extends \Core_Mapper
{

    const TABLE_NAME = DbTable\MailTemplates::TABLE_NAME;

    const PARAM_ID = 'id';
    const PARAM_CODE = 'code';
    const PARAM_NAME = 'name';
    const PARAM_GROUP_ID = 'group_id';
    const PARAM_WITH_CHECK = 'with_check';

    // @codingStandardsIgnoreStart
    protected $_dbClass = 'MailManager\Model\DbTable\MailTemplates';

    public $_parameters = array(
        self::PARAM_ID => array(
            'pseudo' => 'Идентификатор записи',
            'type' => self::TYPE_INT
        ),
        self::PARAM_CODE => array(
            'pseudo' => 'Код',
            'type' => self::TYPE_STRING
        ),
        self::PARAM_NAME => array(
            'pseudo' => 'Название',
            'type' => self::TYPE_STRING
        ),
        self::PARAM_GROUP_ID => array(
            'pseudo' => 'Группа шаблона',
            'type' => self::TYPE_INT
        ),
        self::PARAM_WITH_CHECK => array(
            'pseudo' => 'Флаг для выполнения проверки перед отправкой',
            'type' => self::TYPE_STRING
        )
    );
    // @codingStandardsIgnoreEnd
}
