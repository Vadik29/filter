<?php
/**
 * Подписки на получение подписчиком сообщений, сгенерированных по данному шаблону.
 */

declare(strict_types = 1);

namespace MailManager\Model;

/**
 *  Класс модели.
 */
class MailTemplateSubscription extends \Core_Mapper
{

    const TABLE_NAME = DbTable\MailTemplateGroups::TABLE_NAME;

    const PARAM_ID = 'id';
    const PARAM_SUBSCRIBER_ID = 'subscriber_id';
    const PARAM_TEMPLATE_ID = 'template_id';
    const PARAM_ACTIVE = 'active';

    // @codingStandardsIgnoreStart
    protected $_dbClass = 'MailManager\Model\DbTable\MailTemplateSubscriptions';

    public $_parameters = array(
        self::PARAM_ID => array(
            'pseudo' => 'Идентификатор записи',
            'type' => self::TYPE_INT
        ),
        self::PARAM_SUBSCRIBER_ID => array(
            'pseudo' => 'Идентификатор подписчика',
            'type' => self::TYPE_INT
        ),
        self::PARAM_TEMPLATE_ID => array(
            'pseudo' => 'Идентификатор шаблона',
            'type' => self::TYPE_INT
        ),
        self::PARAM_ACTIVE => array(
            'pseudo' => 'Состояние подписки',
            'type' => self::TYPE_BOOL
        )
    );
    // @codingStandardsIgnoreEnd

    /**
     * Возвращает данные о подписках на шаблоны email-рассылок организации и её пользователей.
     *
     * @param int $contragentId Идентификатор контрагента.
     *
     * @return array
     */
    public function getTemplateSubscriptions(int $contragentId)
    {
        $db = $this->getDbTable()->getAdapter();
        $select = $db->select()->
        from(['mtg' => DbTable\MailTemplateGroups::TABLE_NAME], ['group_name' => MailTemplateGroup::PARAM_NAME])
            ->joinLeft(
                ['mt' => DbTable\MailTemplates::TABLE_NAME],
                sprintf('mt.%s = mtg.%s', MailTemplate::PARAM_GROUP_ID, MailTemplateGroup::PARAM_ID),
                ['tpl_name' => MailTemplate::PARAM_NAME]
            )->joinLeft(
                ['mts' => DbTable\MailTemplateSubscriptions::TABLE_NAME],
                sprintf('mts.%s = mt.%s', self::PARAM_TEMPLATE_ID, MailTemplate::PARAM_ID)
            )->joinLeft(
                ['s' => DbTable\Subscribers::TABLE_NAME],
                sprintf('s.%s = mts.%s', Subscriber::PARAM_ID, self::PARAM_SUBSCRIBER_ID)
            )->joinLeft(
                ['st' => DbTable\SubscriberTypes::TABLE_NAME],
                sprintf('s.%s = st.%s', Subscriber::PARAM_TYPE_ID, SubscriberType::PARAM_ID)
            )->joinLeft(
                ['u' => \DbTable_Users::NAME],
                sprintf(
                    "s.%s = u.%s AND st.%s = '%s'",
                    Subscriber::PARAM_OBJECT_ID,
                    \Model_User::PARAM_ID,
                    SubscriberType::PARAM_CODE,
                    SubscriberType::USER_MAIN_TYPE
                ),
            []
            )->where(sprintf('s.%s = ?', Subscriber::PARAM_OBJECT_ID), $contragentId)
            ->where(
                sprintf('st.%s IN (?)', SubscriberType::PARAM_CODE),
                [SubscriberType::CONTRAGENT_MAIN_TYPE, SubscriberType::CONTRAGENT_ADDITIONAL_TYPE]
            )
            ->orWhere(sprintf('u.%s = ?', \Model_User::PARAM_CONTRAGENT_ID), $contragentId)
            ->order(sprintf('%s %s', self::PARAM_TEMPLATE_ID, \Zend_Db_Select::SQL_ASC));
        $tplSuscriptions = [];
        foreach ($db->fetchAll($select) as $subscription) {
            $tplId = $subscription['template_id'];
            $subscriberId = $subscription['subscriber_id'];
            if (!isset($tplSuscriptions[$tplId])) {
                $tplSuscriptions[$tplId] = [
                    'tpl_id' => $subscription[self::PARAM_TEMPLATE_ID],
                    'tpl_name' => $subscription['tpl_name'],
                    'tpl_group_id' => $subscription[MailTemplate::PARAM_GROUP_ID],
                    'tpl_group_name' => $subscription['group_name'],
                ];
            }
            $tplSuscriptions[$tplId][$subscriberId] = $subscription[self::PARAM_ACTIVE];
        }
        return array_values($tplSuscriptions);
    }

    /**
     * Сохраняет изменения в подписках на шаблоны email-рассылок.
     *
     * @param array $subscriptions Массив обновлений в подписках на шаблоны email-рассылок.
     *
     * @return void
     * @throws \Exception Исключение.
     */
    public function updateSubscriptions(array $subscriptions)
    {
        foreach ($subscriptions as $tplSubscription) {
            $tplId = $tplSubscription['tpl_id'];
            unset($tplSubscription['tpl_id']);
            foreach ($tplSubscription as $subscriberId => $isSubscribed) {
                $this->findOneBy(
                    [self::PARAM_SUBSCRIBER_ID => $subscriberId, self::PARAM_TEMPLATE_ID => $tplId]
                )->update([self::PARAM_ACTIVE => (string)$isSubscribed]);
            }
        }
    }
}
