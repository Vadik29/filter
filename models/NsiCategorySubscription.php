<?php
/**
 * Модель подписки на категориию сферы деятельности.
 */

declare(strict_types = 1);

namespace MailManager\Model;

use Core_Template;
use Nsi\Model\DbMapper\Category;
use PHPUnit\Framework\Exception;

/**
 *  Класс модели.
 */
class NsiCategorySubscription extends \Core_Mapper
{

    const TABLE_NAME = DbTable\NsiCategorySubscriptions::TABLE_NAME;

    const PARAM_ID = 'id';
    const PARAM_SUBSCRIBER_ID = 'subscriber_id';
    const PARAM_CATEGORY_CODE = 'category_code';
    const PARAM_ACTIVE = 'active';

    // @codingStandardsIgnoreStart
    protected $_dbClass = 'MailManager\Model\DbTable\NsiCategorySubscriptions';

    public $_parameters = array(
        self::PARAM_ID => array(
            'pseudo' => 'Идентификатор записи',
            'type' => self::TYPE_INT
        ),
        self::PARAM_SUBSCRIBER_ID => array(
            'pseudo' => 'Идентификатор подписчика',
            'type' => self::TYPE_INT
        ),
        self::PARAM_CATEGORY_CODE => array(
            'pseudo' => 'Код категории сферы деятельности',
            'type' => self::TYPE_STRING
        ),
        self::PARAM_ACTIVE => array(
            'pseudo' => 'Состояние подписки',
            'type' => self::TYPE_BOOL
        )
    );
    // @codingStandardsIgnoreEnd

    /**
     * Модель контрагента.
     *
     * @var \Model_Contragent
     */
    protected $contragent;

    /**
     * Модель пользователя.
     *
     * @var \Model_User
     */
    public $user;

    /**
     * Сервис для работы с шаблонами сообщений.
     *
     * @var Core_Template
     */
    public $templateService;

    /**
     * Модель категорий сфер деятельности.
     *
     * @var Category
     */
    public $nsiCategory;

    /**
     * Модель подписчика.
     *
     * @var Subscriber
     */
    public $subscriber;

    /**
     * Модель типа подписчика.
     *
     * @var SubscriberType
     */
    public $subscriberType;

    /**
     * Сервис категорий сфер деятельности.
     *
     * @var object
     */
    public $nsiCategoryService;

    /**
     * Создаёт экземпляр класса.
     *
     * @param \Model_Contragent  $contragent         Модель контрагента.
     * @param \Model_User        $user               Модель пользователя.
     * @param Core_Template|null $templateService    Сервис для работы с шаблонами сообщений.
     * @param Category|null      $nsiCategory        Модель категорий сфер деятельности.
     * @param Subscriber         $subscriber         Модель подписчика.
     * @param SubscriberType     $subscriberType     Модель типа подписчика.
     * @param object|null        $nsiCategoryService Сервис категории сфер деятельности.
     *
     * @return self $model Экземпляр класса.
     */
    public static function selfCreateObject(
        \Model_Contragent $contragent,
        \Model_User $user,
        Core_Template $templateService = null,
        Category $nsiCategory = null,
        Subscriber $subscriber = null,
        SubscriberType $subscriberType,
        $nsiCategoryService = null
    ) {
        $model = new self();
        $model->contragent = $contragent;
        $model->user = $user;
        $model->templateService = $templateService;
        $model->nsiCategory = $nsiCategory;
        $model->subscriber = $subscriber;
        $model->subscriberType = $subscriberType;
        $model->nsiCategoryService = $nsiCategoryService;
        return $model;
    }

    /**
     * Метод создания модели.
     *
     * @param array $data Данные для создаваемой модели.
     *
     * @return $this
     */
    public static function create(array $data = array())
    {
        $model = new self(null);
        return $model->createInternal($data);
    }

    /**
     * Внутренний метод создания модели.
     *
     * @param array $data Данные для создаваемой модели.
     *
     * @return $this
     */
    protected function createInternal(array $data = array())
    {
        return $this->update($data);
    }

    /**
     * Проверяет есть ли категории НСИ для подписки, по которым организация не прошла аккредитацию.
     *
     * @@param \Model_Contragent $contragent Модель контрагента.
     * @param  array              $categories Коды категорий.
     *
     * @return array $deniedCategories Категории НСИ, по которым организация не прошла аккредитацию.
     */
    public function checkSubscriptionAbility($contragent, array $categories)
    {
        $contragentCategories = $this->nsiCategoryService->getContragentCategoryCodes($contragent);
        $deniedCategories = array_diff($categories, $contragentCategories);
        if ($deniedCategories) {
            $deniedCategories = $this->nsiCategory->
            findBy([Category::PARAM_CODE => array_values($deniedCategories)]);
        }
        return $deniedCategories;
    }

    /**
     * Подписка на категории сфер деятельности.
     *
     * @param Subscriber $subscriber    Модель подписчика.
     * @param array      $nsiCategories Категории справочника сфер деятельности.
     *
     * @return void
     */
    public function subscribe(Subscriber $subscriber, array $nsiCategories)
    {
        foreach ($nsiCategories as $category) {
            if (!$this->updateSubscription($subscriber, $category, true)) {
                $this->create(
                    [
                        self::PARAM_SUBSCRIBER_ID => $subscriber->getId(),
                        self::PARAM_CATEGORY_CODE => $category,
                        self::PARAM_ACTIVE => true
                    ]
                );
            }
        }
    }

    /**
     * Отмена подписки на категории справочника сфер деятельности.
     *
     * @param Subscriber $subscriber    Модель подписчика.
     * @param array      $nsiCategories Категории справочника сфер деятельности.
     *
     * @return void
     */
    public function unsubscribe(Subscriber $subscriber, array $nsiCategories)
    {
        foreach ($nsiCategories as $category) {
            $this->updateSubscription($subscriber, $category, false);
        }
    }

    /**
     * Обновление состояния подписки на категории справочника сфер деятельности.
     *
     * @param Subscriber $subscriber Модель подписчика.
     * @param string     $category   Код категории справочника сфер деятельности.
     * @param bool       $active     Состояние подписки.
     *
     * @return self|null $subscription Модель подписки.
     */
    public function updateSubscription(Subscriber $subscriber, string $category, bool $active)
    {
        $subscription = $this->findOneBy(
            [
                self::PARAM_SUBSCRIBER_ID => $subscriber->getId(),
                self::PARAM_CATEGORY_CODE => $category
            ]
        );
        if ($subscription) {
            $subscription->update([self::PARAM_ACTIVE => $active]);
        }
        return $subscription;
    }

    /**
     * Указание подписок на nsi-категории.
     *
     * @param Subscriber $subscriber           Модель подписчика.
     * @param array      $recivedNsiCategories Категории классификатора сфер деятельности.
     *
     * @return void
     * @throws \Exception Исключение.
     */
    public function setSubscriptions(Subscriber $subscriber, array $recivedNsiCategories)
    {
        $subscriberType = $this->subscriberType->load($subscriber->getTypeId());
        if (in_array(
            $subscriberType->getCode(),
            [SubscriberType::CONTRAGENT_MAIN_TYPE, SubscriberType::CONTRAGENT_ADDITIONAL_TYPE]
        )) {
            $contragent = $this->contragent->load($subscriber->getObjectId());
        } elseif ($subscriberType->getCode() == SubscriberType::USER_MAIN_TYPE) {
            $user = $this->user->load($subscriber->getObjectId());
            $contragent = $this->contragent->load($user->getContragentId());
        } else {
            throw new Exception('MM_CONTRAGENT_OF_SUBSCRIBER_NOT_FOUND');
        }
        $contragentCategories = $this->nsiCategoryService->getContragentCategoryCodes($contragent);
        $subscriptions = $this->
        findBy([self::PARAM_SUBSCRIBER_ID => $subscriber->getId(), self::PARAM_ACTIVE => true])->toArray();
        $unsubscribeCategories = $deleteSubscriptions = [];
        foreach ($subscriptions as $subscription) {
            $categoryCode = $subscription[self::PARAM_CATEGORY_CODE];
            $key = array_search($categoryCode, $recivedNsiCategories);
            if ($key !== false) {
                unset($recivedNsiCategories[$key]);
            } elseif (in_array($categoryCode, $contragentCategories)) {
                $unsubscribeCategories[] = $categoryCode;
            } else {
                $deleteSubscriptions[] = $categoryCode;
            }
        }

        // Проверка доступности категории сферы деятельности на подписку.
        $deniedCategories = $this->checkSubscriptionAbility($contragent, $recivedNsiCategories);
        if ($deniedCategories) {
            $msgAbilityError = $this->templateService->dbProcess(
                Template::MM_NO_ABILITY_NSI_CATEGORY_SUBSCRIPTION,
                ['notAccrededCategories' => $deniedCategories]
            );
            throw new \Exception($msgAbilityError[Core_Template::TEMPLATE_MESSAGE]);
        }

        // Отписка от категорий.
        $this->unsubscribe($subscriber, $unsubscribeCategories);

        // Подписка на категории.
        $this->subscribe($subscriber, $recivedNsiCategories);

        // Удаление подписок на отсутствующие категории в сферах деятельности контрагента.
        $this->deleteSubscruption([$subscriber], $deleteSubscriptions);
    }

    /**
     * Удаляет подписку на категорию сферы деятельности.
     *
     * @param array $subscribers   Подписчики.
     * @param array $categoryCodes Коды категорий.
     *
     * @return void
     */
    public function deleteSubscruption(array $subscribers, array $categoryCodes)
    {
        if (!$subscribers || !$categoryCodes) {
            return;
        }
        $subscriberIds = array_map(
            function($subscriber) {
                return $subscriber->getId();
            },
            $subscribers
        );
        $this->getDbTable()->delete([
            sprintf('%s IN (?)', self::PARAM_SUBSCRIBER_ID) => $subscriberIds,
            sprintf('%s IN (?)', self::PARAM_CATEGORY_CODE) => $categoryCodes,
            sprintf('%s = ?', self::PARAM_ACTIVE) => true
        ]);
    }

    /**
     * Удаляет подписки на категории сфер деятельности у контрагента и его пользователей.
     *
     * @param array             $categoryCodes Код категории.
     * @param \Model_Contragent $contragent    Модель контрагента.
     *
     * @return void
     */
    public function deleteSubscruptionsByContragent(array $categoryCodes, \Model_Contragent $contragent)
    {
        $subscribers = $this->subscriber->getSubscribersByContragent($contragent->getId());
        $this->deleteSubscruption($subscribers, $categoryCodes);
    }
}
