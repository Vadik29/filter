<?php
/**
 * Категории шаблонов сообщений.
 */

declare(strict_types = 1);

namespace MailManager\Model;

/**
 *  Класс модели.
 */
class MailTemplateGroup extends \Core_Mapper
{

    const TABLE_NAME = DbTable\MailTemplateGroups::TABLE_NAME;

    const PARAM_ID = 'id';
    const PARAM_NAME = 'name';
    const PARAM_CODE = 'code';

    // @codingStandardsIgnoreStart
    protected $_dbClass = 'MailManager\Model\DbTable\MailTemplateGroups';

    public $_parameters = array(
      self::PARAM_ID => array(
        'pseudo' => 'Идентификатор записи',
        'type' => self::TYPE_INT
      ),
      self::PARAM_NAME => array(
        'pseudo' => 'Название',
        'type' => self::TYPE_STRING
      ),
      self::PARAM_CODE => array(
        'pseudo' => 'Код',
        'type' => self::TYPE_STRING
      )
    );
    // @codingStandardsIgnoreEnd
}
