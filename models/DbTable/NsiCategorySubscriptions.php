<?php
/**
 * Класс таблицы подписок на категориии сфер деятельности.
 */

declare(strict_types = 1);

namespace MailManager\Model\DbTable;

/**
 * Класс таблицы.
 */
class NsiCategorySubscriptions extends \Zend_Db_Table_Abstract
{

    const TABLE_NAME = 'mm_nsi_category_subscriptions';

    /**
     * Имя таблицы
     *
     * @var string
     */
    // @codingStandardsIgnoreStart
    protected $_name = self::TABLE_NAME;
    // @codingStandardsIgnoreEnd
}
