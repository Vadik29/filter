<?php
/**
 * Класс таблицы групп шаблонов для подписки.
 */

declare(strict_types = 1);

namespace MailManager\Model\DbTable;

/**
 * Класс таблицы.
 */
class MailTemplateGroups extends \Zend_Db_Table_Abstract
{

    const TABLE_NAME = 'mm_mail_template_groups';

    /**
     * Имя таблицы
     *
     * @var string
     */
    // @codingStandardsIgnoreStart
    protected $_name = self::TABLE_NAME;
    // @codingStandardsIgnoreEnd
}
