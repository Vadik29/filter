<?php
/**
 * Класс таблицы типов подписчика.
 */

declare(strict_types = 1);

namespace MailManager\Model\DbTable;

/**
 * Класс таблицы.
 */
class SubscriberTypes extends \Zend_Db_Table_Abstract
{

    const TABLE_NAME = 'mm_subscriber_types';

    /**
     * Имя таблицы
     *
     * @var string
     */
    // @codingStandardsIgnoreStart
    protected $_name = self::TABLE_NAME;
    // @codingStandardsIgnoreEnd
}
