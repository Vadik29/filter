<?php
/**
 * Класс таблицы подписчиков.
 */

declare(strict_types = 1);

namespace MailManager\Model\DbTable;

use MailManager\Model\Subscriber;

/**
 * Класс таблицы.
 */
class Subscribers extends \Zend_Db_Table_Abstract
{

    const TABLE_NAME = 'mm_subscribers';

    /**
     * Имя таблицы
     *
     * @var string
     */
    // @codingStandardsIgnoreStart
    protected $_name = self::TABLE_NAME;
    // @codingStandardsIgnoreEnd
}
