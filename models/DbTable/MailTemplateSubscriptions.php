<?php
/**
 * Класс таблицы подписок на получение сообщений, сгенерированных по шаблонам.
 */

declare(strict_types = 1);

namespace MailManager\Model\DbTable;

/**
 * Класс таблицы.
 */
class MailTemplateSubscriptions extends \Zend_Db_Table_Abstract
{

    const TABLE_NAME = 'mm_mail_template_subscriptions';

    /**
     * Имя таблицы
     *
     * @var string
     */
    // @codingStandardsIgnoreStart
    protected $_name = self::TABLE_NAME;
    // @codingStandardsIgnoreEnd
}
