<?php
/**
 * Класс таблицы шаблонов email сообщений из vocab_doc_templates.
 */

declare(strict_types = 1);

namespace MailManager\Model\DbTable;

/**
 * Класс таблицы.
 */
class MailTemplates extends \Zend_Db_Table_Abstract
{

    const TABLE_NAME = 'mm_mail_templates';

    /**
     * Имя таблицы
     *
     * @var string
     */
    // @codingStandardsIgnoreStart
    protected $_name = self::TABLE_NAME;
    // @codingStandardsIgnoreEnd
}
