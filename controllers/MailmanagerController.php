<?php
/**
 * Контроллер взаимодействия с формой "Фильтр входящих уведомлений".
 */

declare(strict_types = 0);

namespace MailManager;

use MailManager\Model\NsiCategorySubscription as NsiCatSub;
use MailManager\Model\Subscriber;
use MailManager\Model\SubscriberType;
use MailManager\Model\MailTemplateSubscription as MailTplSub;
use MailManager\Model\Service\MailManager;
use Zend_Controller_Request_Abstract;
use Zend_Controller_Response_Abstract;
use Nsi\Model\DbMapper\Category;

/**
 * Контроллер взаимодействия с формой "Фильтр входящих уведомлений".
 */
class MailmanagerController extends \Core_Controller_Action
{
    /**
     * Модель контрагента.
     *
     * @var \Model_Contragent
     */
    public $contragent;

    /**
     * Модель пользователя.
     *
     * @var \Model_User
     */
    public $user;

    /**
     * Модель подписчика.
     *
     * @var Subscriber
     */
    protected $subscriber;

    /**
     * Модель типа подписчиков.
     *
     * @var SubscriberType
     */
    public $subscriberType;

    /**
     * Модель подписки на шаблон email-сообщения.
     *
     * @var MailTplSub
     */
    protected $mailTplSub;

    /**
     * Модель подписки на Nsi категорию.
     *
     * @var NsiCatSub
     */
    protected $nsiCatSub;

    /**
     * Модель категорий продукции.
     *
     * @var Category
     */
    protected $category;

    /**
     * Сервис управления рассылкой.
     *
     * @var MailManager
     */
    protected $mailManagerService;

    /**
     * MailmanagerController constructor.
     *
     * @param Zend_Controller_Request_Abstract  $request            Запрос.
     * @param Zend_Controller_Response_Abstract $response           Ответ.
     * @param array                             $invokeArgs         Параметры.
     * @param \Model_Contragent                 $contragent         Модель контрагента.
     * @param \Model_User                       $user               Модель пользователя.
     * @param Subscriber|null                   $subscriber         Модель подписчика.
     * @param SubscriberType                    $subscriberType     Модель типа подписчика.
     * @param MailTplSub|null                   $mailTplSub         Модель подписки на шаблон email-сообщения.
     * @param NsiCatSub|null                    $nsiCatSub          Модель подписки на Nsi категорию.
     * @param Category|null                     $category           Модель категорий продукции.
     * @param MailManager|null                  $mailManagerService Сервис управления рассылкой.
     */
    public function __construct(
        Zend_Controller_Request_Abstract $request,
        Zend_Controller_Response_Abstract $response,
        array $invokeArgs = array(),
        \Model_Contragent $contragent = null,
        \Model_User $user = null,
        Subscriber $subscriber = null,
        SubscriberType $subscriberType = null,
        MailTplSub $mailTplSub = null,
        NsiCatSub $nsiCatSub = null,
        Category $category = null,
        MailManager $mailManagerService = null
    ) {
        $this->contragent = $contragent;
        $this->user = $user;
        $this->subscriber = $subscriber;
        $this->subscriberType = $subscriberType;
        $this->mailTplSub = $mailTplSub;
        $this->nsiCatSub = $nsiCatSub;
        $this->category = $category;
        $this->mailManagerService = $mailManagerService;
        parent::__construct($request, $response, $invokeArgs);
    }

    /**
     * Загрузка списка подписчиков контрагента и его пользователей.
     *
     * @param array $params Массив параметров.
     *
     * @return void
     * @throws \ResponseException Исключение.
     * @remotable
     */
    public function loadSubscribersAction(array $params)
    {
        try {
            $subscribers = [];
            foreach ($this->subscriber->getSubscribersByContragent(getParamAsInt($params, 'contragentId')) as $subscriber) {
                $subscriberType = $this->subscriberType->load($subscriber->getTypeId());
                $subscriberData = null;
                switch ($subscriberType->getCode()) {
                    case SubscriberType::CONTRAGENT_MAIN_TYPE:
                        $contragent = $this->contragent->load($subscriber->getObjectId());
                        $subscriberData = [
                            'id' => $subscriber->getId(),
                            'name' => t('MM_MAIN_EMAIL'),
                            'email' => $contragent->getEmail(),
                        ];
                        break;
                    case SubscriberType::CONTRAGENT_ADDITIONAL_TYPE:
                        $contragent = $this->contragent->load($subscriber->getObjectId());
                        $subscriberData = [
                            'id' => $subscriber->getId(),
                            'name' => t('MM_ADDITIONAL_EMAIL'),
                            'email' => $contragent->getEmailAdd(),
                        ];
                        break;
                    case SubscriberType::USER_MAIN_TYPE:
                        $user = $this->user->load($subscriber->getObjectId());
                        $subscriberData = [
                            'id' => $subscriber->getId(),
                            'name' => join(' ', [$user->getLastName(), $user->getFirstName(), $user->getMiddleName()]),
                            'email' => $user->getUserEmail(),
                        ];
                        break;
                }
                $subscribers[] = $subscriberData;
            }

            $this->view->result = ['success' => true, 'subscribers' => $subscribers];
        } catch (\Exception $e) {
            throw new \ResponseException($e->getMessage());
        }
    }

    /**
     * Загрузка списка nsi-категорий.
     *
     * @param array $params Параметры.
     *
     * @return void
     * @throws \ResponseException Исключение.
     * @remotable
     */
    public function nsiCategorySubscriptionsLoadAction(array $params)
    {
        try {
            $subscriptions = $this->nsiCatSub->
            findBy(
                [
                    NsiCatSub::PARAM_SUBSCRIBER_ID => getParamAsInt($params, 'subscriberId'),
                    NsiCatSub::PARAM_ACTIVE => true
                ]
            );
            $res = [];
            foreach ($subscriptions as $subscription) {
                $nsiCategory = $this->category->load($subscription->category_code);
                $res[] = ['code' => $nsiCategory->getCode(), 'name' => $nsiCategory->getName()];
            }
            $this->view->data = $res;
            $this->view->totalCount = count($res);
            $this->view->success = true;
        } catch (\Exception $e) {
            throw new \ResponseException($e->getMessage());
        }
    }

    /**
     * Редактирование подписки на nsi-категорию.
     *
     * @param array $params Параметры.
     *
     * @remotable
     * @return void
     * @throws \ResponseException Исключение.
     */
    public function nsiCategorySubscriptionUpdateAction(array $params)
    {
        try {
            $this->nsiCatSub->setSubscriptions(
                $this->subscriber->load(getParamAsInt($params, 'subscriberId')),
                getParamAsArray($params, 'categories')
            );
            $this->view->success = true;
        } catch (\Exception $e) {
            $this->view->success = false;
            throw new \ResponseException($e->getMessage());
        }
    }

    /**
     * Возвращает данные о подписках на шаблоны email-рассылок организации и её пользователей.
     *
     * @param array $params Массив параметров.
     *
     * @return void
     * @throws \ResponseException Исключение.
     * @remotable
     */
    public function templateSubscriptionsLoadAction(array $params)
    {
        try {
            $this->view->data = $this->mailTplSub->
            getTemplateSubscriptions(getParamAsInt($params, 'contragentId'));
            $this->view->totalCount = count($this->view->data);
            $this->view->success = true;
        } catch (\Exception $e) {
            throw new \ResponseException($e->getMessage());
        }
    }

    /**
     * Cохранение данных о подписках на шаблоны email-рассылок.
     *
     * @param array $params Массив параметров.
     *
     * @return void
     * @throws \ResponseException Исключение.
     * @remotable
     */
    public function templateSubscriptionsUpdateAction(array $params)
    {
        try {
            $this->mailTplSub->updateSubscriptions(getParamAsArray($params, 'data'));
            $this->view->data = [];
            $this->view->success = true;
        } catch (\Exception $e) {
            throw new \ResponseException($e->getMessage());
        }
    }
}
