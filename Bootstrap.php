<?php
/**
 * Инициализация модуля управления подписками на email-сообщения.
 *
 * @package MailManager
 */

declare(strict_types = 1);

namespace MailManager;

use Cometp\Models\Events\CompanyEvents;
use Cometp\Models\Events\UserEvents;
use Cometp\Models\Events\MailLogEvents;
use MailManager\Core\Plugins\EventListener;
/**
 * Class Bootstrap
 */
class Bootstrap extends \ZendCom_Application_Module_Bootstrap
{

    /**
     * Инициализация.
     *
     * @return void
     */
    protected function initAutoload()
    {
        $loader = function ($className) {
            if (interface_exists($className, false) ||
                class_exists($className, false) ||
                trait_exists($className, false)) {
                return;
            }
            $className = str_replace('\\', '_', $className);
            \Zend_Loader_Autoloader::autoload($className);
        };
        $autoloader = \Zend_Loader_Autoloader::getInstance();
        $autoloader->pushAutoloader($loader, 'MailManager\\');
    }

    /**
     * Регистрация событий модуля.
     *
     * @return void
     */
    protected function initEvents()
    {
        addListener(CompanyEvents::NEW_CONTRAGENT_CREATED, function (\Model_Contragent $contragent) {
            /** @var EventListener $eventListener */
            $eventListener = $this->getDiContainer()->get('MailManager_Core_Plugins_EventListener');
            $eventListener->onNewContragentRegistered($contragent);
        });

        addListener(UserEvents::NEW_USER_CREATED, function (\Model_User $user) {
            /** @var EventListener $eventListener */
            $eventListener = $this->getDiContainer()->get('MailManager_Core_Plugins_EventListener');
            $eventListener->onNewUserRegistered($user);
        });

        addListener(MailLogEvents::EVENT_NOTIFY_CONTRAGENT_MAIN_ADDR, function (array $params) {
            /** @var EventListener $eventListener */
            $eventListener = $this->getDiContainer()->get('MailManager_Core_Plugins_EventListener');
            return $eventListener->onNotifyContragentMainAddr($params);
        });

        addListener(MailLogEvents::EVENT_NOTIFY_CONTRAGENT_ADD_ADDR, function (array $params) {
            /** @var EventListener $eventListener */
            $eventListener = $this->getDiContainer()->get('MailManager_Core_Plugins_EventListener');
            return $eventListener->onNotifyContragentAddAddr($params);
        });

        addListener(MailLogEvents::EVENT_NOTIFY_CONTRAGENT_USERS, function (array $params) {
            /** @var EventListener $eventListener */
            $eventListener = $this->getDiContainer()->get('MailManager_Core_Plugins_EventListener');
            $eventListener->onNotifyContragentUsers($params);
        });
    }
}
