/**
 * Окно подписки на категории сфер деятельности.
 */

Ext.namespace('Application.components.mailManagerModule');

Ext.define('Application.components.mailManagerModule.NsiCategorySubscriptionsWindow', {
    extend: 'Ext.Window',
    modal: true,
    resizable: false,
    width: 600,

    /**
     * Идентификатор контрагента.
     */
    contragentId: null,

    /**
     * Идентификатор подписчика.
     */
    subscriberId: null,

    /**
     * Инициализация компонента.
     *
     * @return {void}
     */
    initComponent: function () {
        Ext.applyIf(this, {
            title: t('MM_SELECTED_CATEGORIES'),
            items: [{
                xtype: 'Application.components.mailManagerModule.NsiCategorySubscriptionsGrid',
                ref: 'nsiCategorySubGrid',
                subscriberId: this.subscriberId,
                plugins: ['Ext.extension.plugin.RelayEvents'],
                relayedEvents: [
                    {source: this, targetEvents: ['itemselected']}
                ]
            }],
            buttons: [
                {
                    xtype: 'button',
                    text: t('ADD'),
                    scope: this,
                    handler: function() {
                        this.openClassifierWindow()
                    }
                },
                {
                    xtype: 'button',
                    text: t('SAVE'),
                    scope: this,
                    handler: function() {
                        this.saveSelectedCategories()
                    }
                },
                {
                    text: t('CLOSE'),
                    scope: this,
                    handler: function () {
                        this.close();
                    }
                }
            ]
        });
        Application.components.mailManagerModule.NsiCategorySubscriptionsWindow.superclass.initComponent.call(this);
    },

    /**
     * Открывает модальное окно с классификатором.
     *
     * @return {void}
     */
    openClassifierWindow: function () {
        var thisWinPos = this.getPosition();
        var nomenclatureWindow = new Application.components.treeWindow({
            loaderConfig: {
                rootName: getConfigValue('nsi_categories_root', 0),
                textFormat: '{1}',
                getTreeLoader: this.getTreeLoader.createDelegate(this, [this.contragentId], 0)
            },
            title: t('CATEGORY_LIST'),
            keyName: 'code',
            autoSize: false,
            width: 750,
            height: 550,
            modal: true,
            plugins: ['Ext.extension.plugin.RelayEvents'],
            relayedEvents: [
                {target: this, sourceEvents: ['itemselected']}
            ],
            listeners: {
                close: function() {
                    if (this.isVisible()) {
                        this.setPosition(thisWinPos);
                    }
                },
                scope: this
            }
        });
        nomenclatureWindow.show();
        var nomWinPos = nomenclatureWindow.getPosition();
        var del = 2;
        this.setPosition(nomWinPos[0] - this.getWidth() / del, nomWinPos[1]);
        nomenclatureWindow.setPosition(nomWinPos[0] + this.getWidth() / del, nomWinPos[1]);
    },

    /**
     * Открывает модальное окно с классификатором.
     *
     * @return {void}
     */
    saveSelectedCategories: function () {
        var displayParams = {
            mask: true,
            mask_el: this.getEl(),
            scope: this
        };
        var params = {
            subscriberId: this.subscriberId,
            categories: []
        };
        this.nsiCategorySubGrid.getStore().each(function(record) {
            params.categories.push(record.get('code'))
        });
        promiseRPC(RPC_mail_manager.Mailmanager.nsiCategorySubscriptionUpdate, [params], displayParams).then(
            function () {
                this.close();
                Ext.extension.Promise.showMessage({message: t('SUCCESS_DATA_SAVED'), success: true});
            }.createDelegate(this),
            function (response) {
                Ext.extension.Promise.showMessage(response);
            }
        );
    },

    /**
     * Возвращает загрузчик категорий сфер деятельности.
     *
     * @param {Int} contragentId Идентификатор контрагента.
     *
     * @return {object} Загрузчик категорий.
     */
    getTreeLoader: function (contragentId) {
        return new Ext.tree.TreeLoader({
            directFn: function(n, cb) {
                RPC_nsi.Vocab.tree({node: n, type: 'contragent_categories', contragentId: contragentId}, cb)
            }.createDelegate(this)
        });
    }
});

Ext.reg(
    'Application.components.mailManagerModule.NsiCategorySubscriptionsWindow',
    Application.components.mailManagerModule.NsiCategorySubscriptionsWindow
);
