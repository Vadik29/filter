/**
 * Грид подписки на категории сфер деятельности.
 */

Ext.namespace('Application.components.mailManagerModule');

Ext.define('Application.components.mailManagerModule.NsiCategorySubscriptionsGrid', {
    extend: 'Ext.grid.Panel',
    viewConfig: {autoFill: true},
    height: 485,

    /**
     * Идентификатор подписчика.
     */
    subscriberId: null,

    /**
     * Инициализация компонента.
     *
     * @return {void}
     */
    initComponent: function () {
        Ext.applyIf(this, {
            store: this.getGridStore(),
            columns: this.getGridColumns(),
            listeners: {
                itemselected: this.onCategorySelected
            }
        });
        Application.components.mailManagerModule.NsiCategorySubscriptionsGrid.superclass.initComponent.call(this);
    },

    /**
     * Возвращает хранилище грида.
     *
     * @return {Ext.data.DirectStore} Хранилище грида.
     */
    getGridStore: function () {
        return new Ext.data.DirectStore({
            autoLoad: true,
            directFn: RPC_mail_manager.Mailmanager.nsiCategorySubscriptionsLoad,
            baseParams: {subscriberId: this.subscriberId},
            root: 'data',
            idProperty: 'code',
            totalProperty: 'totalCount',
            fields: ['code', 'name']
        });
    },

    /**
     * Возвращает столбцы грида.
     *
     * @return {Array} Столбцы.
     */
    getGridColumns: function () {
        return [
            {
                dataIndex: 'name',
                header: t('FIELD_OF_ACTIVITY'),
                renderer: function(value, metaData, record) {
                    return String.format('{0} {1}', record.get('code'), record.get('name'));
                }
            },
            {
                header: t('OPERATIONS'),
                xtype: 'textactioncolumn',
                actionsSeparator: ' ',
                width: 70,
                fixed: true,
                items: [{
                    tooltip: 'Удалить',
                    icon: '/ico/delete.png',
                    scope: this,
                    handler: function (grid, rowIndex) {
                        grid.getStore().removeAt(rowIndex);
                    }

                }]
            }
        ];
    },

    /**
     * Обработчик нажатия добавления категории.
     *
     * @param {object} categoryNode Категория классификатор.
     *
     * @return {void}
     */
    onCategorySelected: function (categoryNode) {
        var attrs = categoryNode.attributes;
        var store = this.getStore();
        if (store.findExact('code', attrs.code) == INDEX_OF_NOT_FOUND) {
            store.add([new store.recordType({code: attrs.code, name: attrs.name})]);
        } else {
            Ext.Msg.alert(t('COMMON_ERROR'), t('MM_CATEGORY_ALREADY_SELECTED'));
        }
    }
});

Ext.reg(
    'Application.components.mailManagerModule.NsiCategorySubscriptionsGrid',
    Application.components.mailManagerModule.NsiCategorySubscriptionsGrid
);
