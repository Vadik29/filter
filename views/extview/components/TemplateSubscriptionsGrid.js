/**
 * Грид подписок на шаблоны e-mail-сообщений.
 */

Ext.namespace('Application.components.mailManagerModule');

Ext.define('Application.components.mailManagerModule.TemplateSubscriptionsGrid', {
    extend: 'Ext.grid.Panel',
    loadMask: true,
    editable: true,
    enableGridFilter: false,
    groupName: 'tpl_group_name',
    groupValue: 'tpl_name',
    cls: 'mm-template-subscriptions-grid',
    view: new Ext.grid.GroupingView({
        forceFit: true,
        showGroupName: false,
        enableNoGroups: false,
        enableGroupingMenu: false,
        hideGroupedColumn: true,
        getRowClass: function (record) {
            return record.nsiSelector ? 'mm-nsi-category-subscription-selector-row' : '';
        }
    }),

    /**
     * @var {int} Идентификатор контрагента.
     */
    contragentId: undefined,

    /**
     * @var {string} Текст ячейки для выбора подписок на категории сфер деятельности.
     */
    nsiCategorySubText: undefined,

    /**
     * Инициализация компонента.
     *
     * @return {void}
     */
    initComponent: function () {
        Ext.applyIf(this, {
            contragentId: getActiveCompany(),
            nsiCategorySubText: t('SELECT'),
            store: this.getGridStore(),
            columns: this.getGridColumns(),
            bbar: [
                '->',
                {
                    cls: 'x-btn-text-icon',
                    icon: 'ico/undo.png',
                    text: t('CANCEL'),
                    scope: this,
                    handler: function () {
                        this.store.rejectChanges();
                    }
                },
                {
                    cls: 'x-btn-text-icon',
                    icon: 'ico/database_save.png',
                    text: t('SAVE'),
                    scope: this,
                    handler: function () {
                        this.store.save();
                    }
                }]
        });
        this.getStore().setBaseParam('contragentId', this.contragentId);
        this.loadData();
        Application.components.mailManagerModule.TemplateSubscriptionsGrid.superclass.initComponent.call(this);
    },

    /**
     * Загрузка данных в грид.
     *
     * @return {void}
     */
    loadData: function () {
        var params = {
            contragentId: this.contragentId
        };
        this.doSomethingInPanelWithMask(
            function () {
                promiseRPC(RPC_mail_manager.Mailmanager.loadSubscribers, [params]).then(
                    function (response) {
                        this.processSubscribers(response.subscribers);
                    }.createDelegate(this),
                    function (response) {
                        Ext.extension.Promise.showMessage(response);
                    }
                );
            },
            this
        );
    },

    /**
     * Обработка данных о пользователях контрагента.
     *
     * @param {array} subscribers Пользователи-подписчики на рассылки.
     *
     * @return {void}
     */
    processSubscribers: function (subscribers) {
        var subscriberColumns = this.createColumns(subscribers);
        var columns = this.getGridColumns().concat(subscriberColumns);
        var fields = subscriberColumns.map(function (column) {
            return {name: column.dataIndex.toString(), subscriberField: true, defaultValue: false};
        });
        var store = this.getStore();
        var colModel = new Ext.grid.ColumnModel({columns: columns, defaults: {menuDisabled: true}});
        this.reconfigureGrid(store, colModel, fields);
        store.load();
    },

    /**
     * Создаёт столбцы подписчиков вгриде.
     *
     * @param {array} subscribers Подписчики.
     *
     * @return {array} columns Cтолбцы.
     */
    createColumns: function (subscribers) {
        var columns = [],
        tpl = new Application.components.mailManagerModule.Templates();
        Ext.each(subscribers, function (subscriber) {
            columns.push({
                header: tpl.getSubscriberColumnHeaderTpl(subscriber),
                xtype: 'checkactioncolumn',
                dataIndex: subscriber.id,
                handler: this.openNsiCategorySubGrid,
                actionValues: [this.nsiCategorySubText],
                align: 'center'
            });
        },
        this);
        return columns;
    },

    /**
     * Открывает окно выбора категорий сфер деятельности для подписки.
     *
     * @param {Ext.grid.Panel} thisCmp Данный компонент.
     *
     * @returns {void}
     */
    openNsiCategorySubGrid: function (thisCmp) {
        var subWindow = new Application.components.mailManagerModule.NsiCategorySubscriptionsWindow(
            {contragentId: thisCmp.contragentId, subscriberId: this.dataIndex}
        );
        subWindow.show();
    },

    /**
     * Возвращает хранилище грида.
     *
     * @return {Ext.data.DirectStore} Хранилище грида.
     */
    getGridStore: function () {
        return new Ext.data.GroupingStore({
            autoLoad: false,
            proxy: new Ext.data.DirectProxy({
                api: {
                    read: RPC_mail_manager.Mailmanager.templateSubscriptionsLoad,
                    update: RPC_mail_manager.Mailmanager.templateSubscriptionsUpdate
                }
            }),
            reader: this.getStoreReader(),
            writer: this.getStoreWriter(),
            remoteGroup: true,
            groupField: this.groupName,
            listeners: {
                load: this.onStoreLoaded,
                scope: this
            },
            autoSave: false
        });
    },

    /**
     * Построение таблицы с учетом динамических столбцов кросстаблицы.
     *
     * @param {Ext.data.DirectStore} store Хранилище.
     * @param {Ext.grid.ColumnModel} colModel Конфигурация столбцов.
     * @param {Array} [fields] Поля кросстаблицы хранилища.
     *
     * @return {void}
     */
    reconfigureGrid: function (store, colModel, fields) {
        store.reader = this.getStoreReader(fields);
        store.fields.addAll(fields);
        this.reconfigure(store, colModel);
    },

    /**
     * Ридер хранилища с применением группировки.
     *
     * @param {Array} [fields] Передваемые поля.
     *
     * @return {Ext.data.JsonReader} Ридер.
     */
    getStoreReader: function (fields) {
        fields = [
            {name: 'tpl_id'},
            {name: 'tpl_group_id'},
            {name: this.groupName},
            {name: this.groupValue}
        ].concat(fields || []);

        return new Ext.data.JsonReader({
            idProperty: 'tpl_id',
            fields: fields,
            root: 'data',
            totalProperty: 'totalCount'
        });
    },

    /**
     * Возвращает врайтер хранилища.
     *
     * @return {Ext.data.JsonWriter} Врайтер.
     */
    getStoreWriter: function () {
        return new Ext.data.JsonWriter(
            {
                encode: false,
                listful: true
            }
        );
    },

    /**
     * Возвращает столбцы грида.
     *
     * @return {Array} Столбцы.
     */
    getGridColumns: function () {
        return [
            {dataIndex: this.groupValue, header: t('MM_TPL_NAME'), width: 300},
            {dataIndex: this.groupName, hidden: true}
        ];
    },

    /**
     * Обработчик события загрузки данных в хранилище.
     *
     * @param {Ext.data.DirectStore} store Хранилище.
     *
     * @return {void}
     */
    onStoreLoaded: function (store) {
        this.addNsiCategorySubscriptions(store)
    },

    /**
     * Добавляет строку в грид для управления подпиской на сферы деятельности.
     *
     * @param {Ext.data.DirectStore} store Хранилище.
     *
     * @return {void}
     */
    addNsiCategorySubscriptions: function (store) {
        var $recordData = {
            tpl_group_id: Ext.max(store.getRange().map(
                function (row) {
                    return row.get('tpl_group_id');
                }
            )) + 1
        };
        Ext.each(this.getStore().reader.meta.fields, function (field) {
            if (field.subscriberField) {
                $recordData[field.name] = this.nsiCategorySubText;
            }
        }, this);
        $recordData[this.groupName] = t('MM_NOTIFICATIONS_CONTAINING_NSI_CATEGORIES');
        $recordData[this.groupValue] = t('MM_SELECT_NSI_CATEGORIES');
        var record = new store.recordType($recordData);
        record.nsiSelector = true;
        record.phantom = false;
        store.add([record]);
    }
});

Ext.reg(
    'Application.components.mailManagerModule.TemplateSubscriptionsGrid',
    Application.components.mailManagerModule.TemplateSubscriptionsGrid
);
