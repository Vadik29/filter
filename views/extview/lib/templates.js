/**
 * Объект для работы с HTML-шаблонами.
 */

Ext.namespace('Application.components.mailManagerModule');

Ext.define('Application.components.mailManagerModule.Templates', {
    extend: 'Ext.util.Observable',

    /**
     * Возвращает текст заголовка в таблице подписчиков.
     *
     * @param {Object} subscriber Данные подписчика.
     *
     * @return {text} Заголовок.
     */
    getSubscriberColumnHeaderTpl: function(subscriber) {
        return new Ext.XTemplate(
            '{name}<br /><tpl if="email">{email}</tpl><tpl if="!email">' + t('MM_NOT_SPECIFIED') + '</tpl>'
        ).apply(subscriber);
    }
});

Ext.reg('Application.components.mailManagerModule.Templates', Application.components.mailManagerModule.Templates);
