Ext.ns('Application.controllers.mailManagerModule');

Application.controllers.mailManagerModule.MailManagerController = Ext.extend(Application.controllers.Abstract, {

    /**
     * Страница настройки фильтра уведомлений.
     *
     * @param {Object} params [null] - хэш с переданными параметрами
     * @param {Object} app [null] - ссылка на объект приложения
     * @param {Object} panel [null] - ссылка на объект ViewPort'a
     *
     * @returns {void}
     */
    settingsAction: function(params, app, panel) {
        panel.add({
            xtype: 'Application.components.fullscreenPanel',
            cmpType: 'Application.components.mailManagerModule.TemplateSubscriptionsGrid',
            title: t('MM_TEMPLATE_SUBSCRIPTION')
        });
    }
});
